package api

import (
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/labstack/echo/v4"
)

type MonapiAPI struct {
	// monapi datas
	config ApiConfiguration
	// lockable
	Lock sync.Mutex
}

func NewMonapiAPI(config ApiConfiguration) *MonapiAPI {
	return &MonapiAPI{
		config: config,
	}
}

func (p *MonapiAPI) GetMonapi(ctx echo.Context) error {
	// We're always asynchronous, so lock unsafe operations below if needed
	// p.Lock.Lock()
	// defer p.Lock.Unlock()
	// 1. wait for response delay - ResponseDelay from config
	time.Sleep(p.config.ResponseDelay)
	// 2. create big response (A..Z loop) - ResponseSize from config
	var b strings.Builder
	b.Grow(p.config.ResponseSize)
	for i := 0; i < p.config.ResponseSize; i++ {
		b.WriteByte(byte(65 + i%26))
	}
	s := b.String()
	// 3. return response
	return ctx.String(http.StatusOK, s)
}

func (p *MonapiAPI) GetHealthz(ctx echo.Context) error {
	// We're always asynchronous, so lock unsafe operations below if needed
	// p.Lock.Lock()
	// defer p.Lock.Unlock()
	return ctx.JSON(http.StatusOK, "OK")
}
