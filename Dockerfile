# --------------------- BUILD STAGE ------------------------

FROM cgr.dev/chainguard/go:latest AS builder

WORKDIR /work

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

# copy the rest of the source code and build
COPY . .
RUN go build -o /work/monapi .

# ----------------------- FINAL IMAGE ----------------------

FROM cgr.dev/chainguard/glibc-dynamic:latest
LABEL org.opencontainers.image.description="Mon API"
LABEL org.opencontainers.image.source="https://github.com/repo/monapi"

WORKDIR /

COPY --from=builder /work/monapi /monapi

CMD ["/monapi"]
